<?php
namespace App\Http\Controllers;
use App\Book;

class BookController extends Controller {
    public function list() {
        $books = Book::paginate(10);
        return view('book.list', compact('books'));
    }

    public function create() {

    }

    public function save() {

    }

    public function edit($id) {

    }

    public function delete($id) {

    }

    public function view($id) {

    }
}
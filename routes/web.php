<?php

use Illuminate\Support\Facades\Route;

Route::prefix('book')->group(function() {
    Route::get('/list', 'BookController@list');
    Route::get('/create', 'BookController@create');
    Route::post('/save', 'BookController@save');
    Route::get('/edit/{id}', 'BookController@edit');
    Route::get('/delete/{id}', 'BookController@delete');
    Route::get('/view/{id}', 'BookController@view');
});

Route::prefix('note')->group(function() {
    Route::get('/list', 'NoteController@list');
    Route::get('/create', 'NoteController@create');
    Route::post('/save', 'NoteController@save');
    Route::get('/edit/{id}', 'NoteController@edit');
    Route::get('/delete/{id}', 'NoteController@delete');
});


Route::get('/', function () {
    return view('home');
});

Route::get('/login', function() {
    return view('login'); // login.blade.php
});

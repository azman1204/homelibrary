@extends('master')
@section('content')

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>No</th>
            <th>Title</th>
            <th>Publisher</th>
            <th>Author</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @php $no = $books->firstItem() @endphp
        @foreach ($books as $book)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $book->title }}</td>
            <td>{{ $book->publisher }}</td>
            <td>{{ $book->author }}</td>
            <td>
                <a href="{{ url('/book/edit/' . $book->id) }}"><i class='fa fa-edit'></i></a>
                <a href="{{ url('/book/delete/' . $book->id) }}"><i class='fa fa-trash'></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection